
;(function () {
    'use strict';
    angular.module('app.basket')
        .controller('BasketController', BasketController);

    function BasketController($log, $state, BasketResource) {
        $log.info(BasketController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.products = {};
        vm.pictures = {};

        vm.$$ui = {
            loader: true

        };

        vm.$$ix = {
            remove : removeBasketItemAction
        };
        
        switch ($state.current.name) {
            case 'basket':
                vm.products = getItemsAction();
                vm.$$ix.remove = removeBasketItemAction;
                break;
            default:
                break;
        }


        function getItemsAction(){
            $log.info(getItemsAction.name);

            var params = {
                id: $state.params.id
            };
            return BasketResource
                .get(params, success, error);

            function error(error) {
                $log.error(getItemsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getItemsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }


        function removeBasketItemAction(index) {
            $log.info(removeBasketItemAction.name, '$index:', index, vm.products);

            var post = vm.products.splice(index, 1).pop(); // Remove object from array and return removed object.
            $log.log(removeBasketItemAction.name, 'Removing post:', post);

            $log.log(post.$remove(success, error));

            function error(error) {
                $log.error(removeBasketItemAction.name, 'ERROR', 'error:', error);
                vm.products.splice(index, 0, post); // Restore removed object to array at same position.
            }

            function success(resource, responseHeader) {
                $log.log(removeBasketItemAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }
        }

    }

})();


