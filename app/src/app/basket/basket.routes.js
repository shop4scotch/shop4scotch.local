
;(function () {
    'use strict';

    angular.module('app.basket')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('basket', {
                cache: false, // false will reload on every visit.
                controller: 'BasketController as vm',
                templateUrl: 'html/basket/basket.view.html',
                url: '/basket'
            });
    }

})();