
;(function () {
    'use strict';

    angular.module('app.product')
        .config(Routes);

    /!* @ngInject *!/
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('products.show', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/product.view.html',
                url: '/product/{id:int}/'
            })
            
            .state('products', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/products.view.html'
            })
        .state('products.grid', {
            cache: false, // false will reload on every visit.
            controller: 'ProductsController as vm',
            templateUrl: 'html/products/products_grid.partial.html',
            url: '/products-grid/{category_id:int}/'
        })
        
    }

})();
