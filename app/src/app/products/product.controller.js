
;(function () {
    'use strict';
    angular.module('app.product')
        .controller('ProductsController', ProductsController);

    function ProductsController($log, $state, ProductsResource, ProductResource, BasketResource, productsCategoryResource) {
        $log.info(ProductsController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.product = new BasketResource();
        vm.products = {};
        vm.pictures = {};
        vm.categories = {};

        vm.$$ix = {
            save : createBasketItemAction
        };

        vm.$$ui = {
            loader: true
            
        };

        vm.item = {
          basket_id: 3, 
          product_id: $state.params.id
        };
        


        switch ($state.current.name) {
            case 'products.grid':
                vm.products = getProductsAction();
                vm.categories = getCategoryAction();
                break;
            case 'products.show':
                vm.categories = getCategoryAction();
                vm.product = getProductAction();
                vm.$$ix.save = createBasketItemAction;
                break;
            default:
                break;
        }

        function createBasketItemAction() {
            $log.info(createBasketItemAction.name, 'product:', vm.item);

            return BasketResource
                .save(vm.item, success, error);

            function error(error) {
                $log.error(createBasketItemAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(createBasketItemAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
            }
        }
        
        

        function getProductAction(){
            $log.info(getProductAction.name);

            var params = {
                id: $state.params.id
            };
            return ProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getCategoryAction(){
            $log.info(getCategoryAction.name);

            var params = {
                id: $state.params.id
            };
            return productsCategoryResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCategoryAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getCategoryAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getProductsAction(){
            $log.info(getProductsAction.name);

            var params = {
                category_id: $state.params.category_id
            };
            return ProductsResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }
    }

})();