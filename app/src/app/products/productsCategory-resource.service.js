/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.product')
        .factory('productsCategoryResource', productsCategoryResource);

    /* @ngInject */
    function productsCategoryResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('categories');


        var paramDefaults = {
            category_id : '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true

            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
