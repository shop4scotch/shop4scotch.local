
;(function () {
    'use strict';

    angular.module('app.profile')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('profile', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/profile.view.html',
                url: '/profile'
            })
            .state('wishlist', {
            cache: false, // false will reload on every visit.
            controller: 'ProfileController as vm',
            templateUrl: 'html/profile/wishlist.view.html',
            url: '/profile/wishlist'
            })
            .state('orderhistory', {
            cache: false, // false will reload on every visit.
            controller: 'ProfileController as vm',
            templateUrl: 'html/profile/orderhistory.view.html',
            url: '/profile/orderhistory'
            });
    }

})();