
;(function () {
    'use strict';

    angular.module('app.profile')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController(
        // Angular
        $log
    ) {
        $log.info('ProfileController', Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};


        // User Interface
        // --------------
        vm.$$ui = {

            user: [
                {
                    firstname: "Sam",
                    lastname:"De Smedt",
                    gender: "male",
                    email: "desmedtsam96@gmail.com",
                    phone:"0472511634"
                }

                ],

            product: [
                {
                    title: "Islay Blended Malt",
                    description: "The Isle of Islay is known for its peaty whiskies. For there is a great abundance of peat on the island, and because electricity reached Islay so late, peated was relied upon as a staple source of fuel. But there is so much than just peat to be found.",
                    price: "103.45",
                    characters:
                        [
                            { name:"honey"},
                            { name:"chocolate"},
                            { name:"vanilla"},
                            { name:"mango"}
                        ],
                    canPurchase: true,
                    //image: "../css/images/example.jpg"
                },

                {
                    title: "Springbank 10 Year old",
                    description: "Blended from a mixture of bourbon and sherry casks, the light colour of this malt belies the richness of its character.",
                    price: "36.25",
                    canPurchase: true
                },

                {
                    title: "Hazelburn 10 Year old",
                    description: "First released in 2014, this is the first bottling of Hazelburn at 10 years of age. Hazelburn is Springbank's triple-distilled, unpeated single malt.",
                    price: "37.75",
                    canPurchase: true
                },

                {
                    title: "Hibiki Japanese Harmony ",
                    description: "Harmony - a rather apt word to describe this well-balanced Japanese blended whisky from the Hibiki range. Hibiki Japanese Harmony is made with malt whiskies from the Yamazaki and Hakushu distilleries, as well as grain whisky from the Chita distillery. ",
                    price: "63.25",
                    canPurchase: true

                },

                {
                    title: "Jack Daniel's 2011 Birthday Edition",
                    description: "The 2011 special edition of Jack Daniel's, bottled in honour of Mr Daniel's 161st birthday. This is their controversial changed bottle shape with a black wrap, the first of the new bottles that we've had in.",
                    price: "39.95",
                    canPurchase: true
                },

                {
                    title: "Heaven Hill",
                    description: "The flagship brand produced by the Heaven Hill distillery in Kentucky. A solid 4 year old bourbon.",
                    price: "19.95",
                    canPurchase: true
                }
            ],

            orderhistory: [
                {
                    ordernumber: "001",
                    date: "05/04/2016",
                    title: "Hibiki Japanese Harmony",
                    delivered: "yes"
                },

                {
                    ordernumber: "002",
                    date: "12/06/2016",
                    title: "Heaven Hill",
                    delivered: "yes"
                }

            ]



        }
    }

})();




