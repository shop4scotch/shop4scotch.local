/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    // ===================
    angular.module('app', [
        // Angular Module Dependencies
        // ---------------------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        // Third-party Module Dependencies
        // -------------------------------
        'ui.router', // Angular UI Router: https://github.com/angular-ui/ui-router/wiki

        // Custom Module Dependencies
        // --------------------------
        'app.home',
        'app.product',
        'app.basket',
        'app.profile',
        'app.services',
        'app.style-guide'
    ]);

    angular.module('app.home', []);
    angular.module('app.product', []);
    angular.module('app.basket', []);
    angular.module('app.profile', []);
    angular.module('app.services', []);
    angular.module('app.style-guide', []);


})();