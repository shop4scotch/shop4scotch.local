
;(function () {
    'use strict';

    angular.module('app.home')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('home', {
                cache: false, // false will reload on every visit.
                controller: 'HomeController as vm',
                templateUrl: 'html/home/home.view.html',
                url: '/'
            });
    }

})();