
;(function () {
    'use strict';
    angular.module('app.home')
        .controller('HomeController', HomeController);

    function HomeController($log, $state, homeProductResource, CategoryResource) {
        $log.info(HomeController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.products = {};
        vm.pictures = {};
        vm.categories = {};

        vm.$$ui = {
            loader: true

        };
        
        
        switch ($state.current.name) {
            case 'home':
                vm.products = getProductsAction();
                vm.categories = getCategoryAction();
            default:
                break;
        }


        function getProductsAction(){
            $log.info(getProductsAction.name);

            var params = {
                id: $state.params.id
            };
            return homeProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getCategoryAction(){
            $log.info(getCategoryAction.name);

            var params = {
                id: $state.params.id
            };
            return CategoryResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCategoryAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getCategoryAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }


    }

})();


