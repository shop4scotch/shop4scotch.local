/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.home')
        .factory('homeProductResource', homeProductResource);

    /* @ngInject */
    function homeProductResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('product/:id');
           

        var paramDefaults = {
           id: '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true

            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
