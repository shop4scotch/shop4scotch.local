/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    // ===================
    angular.module('app', [
        // Angular Module Dependencies
        // ---------------------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        // Third-party Module Dependencies
        // -------------------------------
        'ui.router', // Angular UI Router: https://github.com/angular-ui/ui-router/wiki

        // Custom Module Dependencies
        // --------------------------
        'app.home',
        'app.product',
        'app.basket',
        'app.profile',
        'app.services',
        'app.style-guide'
    ]);

    angular.module('app.home', []);
    angular.module('app.product', []);
    angular.module('app.basket', []);
    angular.module('app.profile', []);
    angular.module('app.services', []);
    angular.module('app.style-guide', []);


})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Config.$inject = ['$compileProvider', '$mdThemingProvider'];
    angular.module('app')
        .config(Config);

    /* @ngInject */
    function Config(
        // Angular
        $compileProvider,
        // Angular Material Design
        $mdThemingProvider
    ) {
        // Debugging
        // ---------
        var debug = true; // Set to `false` for production
        $compileProvider.debugInfoEnabled(debug);

        // Angular Material Design
        // -----------------------
        // All built-in colour palettes
        var colour = {
            amber     : 'amber',
            blue      : 'blue',
            blueGrey  : 'blue-grey',
            brown     : 'brown',
            cyan      : 'cyan',
            deepOrange: 'deep-orange',
            deepPurple: 'deep-purple',
            green     : 'green',
            grey      : 'grey',
            indigo    : 'indigo',
            lightBlue : 'light-blue',
            lightGreen: 'light-green',
            lime      : 'lime',
            orange    : 'orange',
            pink      : 'pink',
            purple    : 'purple',
            red       : 'red',
            teal      : 'teal',
            yellow    : 'yellow'
        };

        // `default` theme configuration
        $mdThemingProvider.theme('default')
            .primaryPalette(colour.blue);

        // `alternative` theme configuration
        $mdThemingProvider.theme('alternative')
            .dark()
            .primaryPalette(colour.blue)
            .accentPalette(colour.lime)
            .warnPalette(colour.pink)
        ;
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    var secure = false;

    angular.module('app')
        .constant('CONFIG', {
            api: {
                protocol: secure ? 'https' : 'http',
                host    : 'www.shop4scotch.local',
                path    : '/api/v1/'
            }
        });
})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    Routes.$inject = ['$urlRouterProvider'];
    angular.module('app')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $urlRouterProvider
    ) {
        //$urlRouterProvider.otherwise('/');
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    BasketResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.basket')
        .factory('BasketResource', BasketResource);

    /* @ngInject */
    function BasketResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('basket_products/:id');


        var paramDefaults = {
            id: '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


;(function () {
    'use strict';
    BasketController.$inject = ['$log', '$state', 'BasketResource'];
    angular.module('app.basket')
        .controller('BasketController', BasketController);

    function BasketController($log, $state, BasketResource) {
        $log.info(BasketController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.products = {};
        vm.pictures = {};

        vm.$$ui = {
            loader: true

        };

        vm.$$ix = {
            remove : removeBasketItemAction
        };
        
        switch ($state.current.name) {
            case 'basket':
                vm.products = getItemsAction();
                vm.$$ix.remove = removeBasketItemAction;
                break;
            default:
                break;
        }


        function getItemsAction(){
            $log.info(getItemsAction.name);

            var params = {
                id: $state.params.id
            };
            return BasketResource
                .get(params, success, error);

            function error(error) {
                $log.error(getItemsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getItemsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }


        function removeBasketItemAction(index) {
            $log.info(removeBasketItemAction.name, '$index:', index, vm.products);

            var post = vm.products.splice(index, 1).pop(); // Remove object from array and return removed object.
            $log.log(removeBasketItemAction.name, 'Removing post:', post);

            $log.log(post.$remove(success, error));

            function error(error) {
                $log.error(removeBasketItemAction.name, 'ERROR', 'error:', error);
                vm.products.splice(index, 0, post); // Restore removed object to array at same position.
            }

            function success(resource, responseHeader) {
                $log.log(removeBasketItemAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());

            }
        }

    }

})();




;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.basket')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('basket', {
                cache: false, // false will reload on every visit.
                controller: 'BasketController as vm',
                templateUrl: 'html/basket/basket.view.html',
                url: '/basket'
            });
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    CategoryResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.home')
        .factory('CategoryResource', CategoryResource);

    /* @ngInject */
    function CategoryResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('categories');


        var paramDefaults = {
            category_id : '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true

            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


;(function () {
    'use strict';
    HomeController.$inject = ['$log', '$state', 'homeProductResource', 'CategoryResource'];
    angular.module('app.home')
        .controller('HomeController', HomeController);

    function HomeController($log, $state, homeProductResource, CategoryResource) {
        $log.info(HomeController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.products = {};
        vm.pictures = {};
        vm.categories = {};

        vm.$$ui = {
            loader: true

        };
        
        
        switch ($state.current.name) {
            case 'home':
                vm.products = getProductsAction();
                vm.categories = getCategoryAction();
            default:
                break;
        }


        function getProductsAction(){
            $log.info(getProductsAction.name);

            var params = {
                id: $state.params.id
            };
            return homeProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getCategoryAction(){
            $log.info(getCategoryAction.name);

            var params = {
                id: $state.params.id
            };
            return CategoryResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCategoryAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getCategoryAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }


    }

})();




;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.home')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('home', {
                cache: false, // false will reload on every visit.
                controller: 'HomeController as vm',
                templateUrl: 'html/home/home.view.html',
                url: '/'
            });
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    homeProductResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.home')
        .factory('homeProductResource', homeProductResource);

    /* @ngInject */
    function homeProductResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('product/:id');
           

        var paramDefaults = {
           id: '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true

            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    ProductResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.product')
        .factory('ProductResource', ProductResource);

    /* @ngInject */
    function ProductResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('product/:id');
           

        var paramDefaults = {
            id: '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: false

            },

            'save': {
                method: 'POST'
            },

            'update': {
                method: 'PUT'
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


;(function () {
    'use strict';
    ProductsController.$inject = ['$log', '$state', 'ProductsResource', 'ProductResource', 'BasketResource', 'productsCategoryResource'];
    angular.module('app.product')
        .controller('ProductsController', ProductsController);

    function ProductsController($log, $state, ProductsResource, ProductResource, BasketResource, productsCategoryResource) {
        $log.info(ProductsController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;
        vm.product = new BasketResource();
        vm.products = {};
        vm.pictures = {};
        vm.categories = {};

        vm.$$ix = {
            save : createBasketItemAction
        };

        vm.$$ui = {
            loader: true
            
        };

        vm.item = {
          basket_id: 3, 
          product_id: $state.params.id
        };
        


        switch ($state.current.name) {
            case 'products.grid':
                vm.products = getProductsAction();
                vm.categories = getCategoryAction();
                break;
            case 'products.show':
                vm.categories = getCategoryAction();
                vm.product = getProductAction();
                vm.$$ix.save = createBasketItemAction;
                break;
            default:
                break;
        }

        function createBasketItemAction() {
            $log.info(createBasketItemAction.name, 'product:', vm.item);

            return BasketResource
                .save(vm.item, success, error);

            function error(error) {
                $log.error(createBasketItemAction.name, 'ERROR', 'error:', error);
            }

            function success(resource, responseHeader) {
                $log.log(createBasketItemAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
            }
        }
        
        

        function getProductAction(){
            $log.info(getProductAction.name);

            var params = {
                id: $state.params.id
            };
            return ProductResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getCategoryAction(){
            $log.info(getCategoryAction.name);

            var params = {
                id: $state.params.id
            };
            return productsCategoryResource
                .get(params, success, error);

            function error(error) {
                $log.error(getCategoryAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getCategoryAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }

        function getProductsAction(){
            $log.info(getProductsAction.name);

            var params = {
                category_id: $state.params.category_id
            };
            return ProductsResource
                .get(params, success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;

            }
        }
    }

})();

;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.product')
        .config(Routes);

    /!* @ngInject *!/
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('products.show', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/product.view.html',
                url: '/product/{id:int}/'
            })
            
            .state('products', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/products.view.html'
            })
        .state('products.grid', {
            cache: false, // false will reload on every visit.
            controller: 'ProductsController as vm',
            templateUrl: 'html/products/products_grid.partial.html',
            url: '/products-grid/{category_id:int}/'
        })
        
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    ProductsResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.product')
        .factory('ProductsResource', ProductsResource);

    /* @ngInject */
    function ProductsResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('products/:category_id');
           

        var paramDefaults = {
            category_id: '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    productsCategoryResource.$inject = ['$cacheFactory', '$resource', 'UriService'];
    angular.module('app.product')
        .factory('productsCategoryResource', productsCategoryResource);

    /* @ngInject */
    function productsCategoryResource(
        // Angular
        $cacheFactory,
        $resource,
        // Custom
        UriService
    ) {
        var url = UriService.getApi('categories');


        var paramDefaults = {
            category_id : '@id'
        };

        var actions = {
            'get': {
                method: 'GET',
                isArray: true

            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();


/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    lodash.$inject = ['$window'];
    angular.module('app.services')
        .factory('$_', lodash);

    /* @ngInject */
    function lodash(
        // Angular
        $window
    ) {
        var _ = $window._;

        delete window._;
        delete $window._;

        return _;
    }

})();
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    UriService.$inject = ['$location', 'CONFIG'];
    angular.module('app.services')
        .factory('UriService', UriService);

    /* @ngInject */
    function UriService(
        // Angular
        $location,
        // Custom
        CONFIG
    ) {
        return {
            getApi: getApi
        };

        function getApi(path) {
            var protocol = CONFIG.api.protocol ? CONFIG.api.protocol : $location.protocol(),
                host     = CONFIG.api.host     ? CONFIG.api.host     : $location.host(),
                uri      = protocol + '://' + host + CONFIG.api.path + path;

            return uri;
        }

    }

})();


;(function () {
    'use strict';

    ProfileController.$inject = ['$log'];
    angular.module('app.profile')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController(
        // Angular
        $log
    ) {
        $log.info('ProfileController', Date.now());

        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {};


        // User Interface
        // --------------
        vm.$$ui = {

            user: [
                {
                    firstname: "Sam",
                    lastname:"De Smedt",
                    gender: "male",
                    email: "desmedtsam96@gmail.com",
                    phone:"0472511634"
                }

                ],

            product: [
                {
                    title: "Islay Blended Malt",
                    description: "The Isle of Islay is known for its peaty whiskies. For there is a great abundance of peat on the island, and because electricity reached Islay so late, peated was relied upon as a staple source of fuel. But there is so much than just peat to be found.",
                    price: "103.45",
                    characters:
                        [
                            { name:"honey"},
                            { name:"chocolate"},
                            { name:"vanilla"},
                            { name:"mango"}
                        ],
                    canPurchase: true,
                    //image: "../css/images/example.jpg"
                },

                {
                    title: "Springbank 10 Year old",
                    description: "Blended from a mixture of bourbon and sherry casks, the light colour of this malt belies the richness of its character.",
                    price: "36.25",
                    canPurchase: true
                },

                {
                    title: "Hazelburn 10 Year old",
                    description: "First released in 2014, this is the first bottling of Hazelburn at 10 years of age. Hazelburn is Springbank's triple-distilled, unpeated single malt.",
                    price: "37.75",
                    canPurchase: true
                },

                {
                    title: "Hibiki Japanese Harmony ",
                    description: "Harmony - a rather apt word to describe this well-balanced Japanese blended whisky from the Hibiki range. Hibiki Japanese Harmony is made with malt whiskies from the Yamazaki and Hakushu distilleries, as well as grain whisky from the Chita distillery. ",
                    price: "63.25",
                    canPurchase: true

                },

                {
                    title: "Jack Daniel's 2011 Birthday Edition",
                    description: "The 2011 special edition of Jack Daniel's, bottled in honour of Mr Daniel's 161st birthday. This is their controversial changed bottle shape with a black wrap, the first of the new bottles that we've had in.",
                    price: "39.95",
                    canPurchase: true
                },

                {
                    title: "Heaven Hill",
                    description: "The flagship brand produced by the Heaven Hill distillery in Kentucky. A solid 4 year old bourbon.",
                    price: "19.95",
                    canPurchase: true
                }
            ],

            orderhistory: [
                {
                    ordernumber: "001",
                    date: "05/04/2016",
                    title: "Hibiki Japanese Harmony",
                    delivered: "yes"
                },

                {
                    ordernumber: "002",
                    date: "12/06/2016",
                    title: "Heaven Hill",
                    delivered: "yes"
                }

            ]



        }
    }

})();






;(function () {
    'use strict';

    Routes.$inject = ['$stateProvider'];
    angular.module('app.profile')
        .config(Routes);

    /* @ngInject */
    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('profile', {
                cache: false, // false will reload on every visit.
                controller: 'ProfileController as vm',
                templateUrl: 'html/profile/profile.view.html',
                url: '/profile'
            })
            .state('wishlist', {
            cache: false, // false will reload on every visit.
            controller: 'ProfileController as vm',
            templateUrl: 'html/profile/wishlist.view.html',
            url: '/profile/wishlist'
            })
            .state('orderhistory', {
            cache: false, // false will reload on every visit.
            controller: 'ProfileController as vm',
            templateUrl: 'html/profile/orderhistory.view.html',
            url: '/profile/orderhistory'
            });
    }

})();