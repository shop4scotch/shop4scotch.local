/**
 * Created by samdesmedt on 04/05/16.
 */

(gulp => {
    'use strict';

const CFG = global.CONFIG;

let exec = require('child_process').exec;

gulp.task('default', [
    'default:remove',
    'vendor',
    'markup',
    'scripts',
    'styles'
]);

gulp.task('default:remove', () => {
    exec(`rm -rf ${CFG.dir.dest}`);
});

})(require('gulp'));