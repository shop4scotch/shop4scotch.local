@extends('layouts.app')

@section('header')
    {{--bootstrap select--}}
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                {!! Form::model($category, ['route' => ['backoffice.categories.store'], 'enctype' => 'multipart/form-data']) !!}

                <div class="panel panel-default">
                    <div class="panel-heading">Create a New Category</div>
                    <div class="panel-body">

                        {{--general information--}}
                        <fieldset>
                            <legend id="legend">New Category Name</legend>

                            <div class="form-group col-md-12{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label($name = 'name', $value = 'Name', $options = [
                                    'class' => 'control-label',
                                ]) !!}
                                {!! Form::text($name = 'name', $value = null, $options = [
                                    'class' => 'form-control',
                                    'id' => 'name',
                                    'placeholder' => 'Name of the category',
                                    'required' => '',
                                ]) !!}
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label($name = 'description', $value = 'Description', $options = [
                                    'class' => 'control-label',
                                ]) !!}
                                {!! Form::textarea($name = 'description', $value = null, $options = [
                                    'class' => 'form-control',
                                    'id' => 'description',
                                    'rows' => '5',
                                    'placeholder' => 'Description for the category',
                                    'required' => '',
                                ]) !!}
                            </div>
                        </fieldset>
                        <br>
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit($value = 'Save', $options = [
                            'class' => 'btn btn-primary',
                        ]) !!}
                        {!! link_to_route($name = 'backoffice.categories.index', $title = 'Cancel', $parameters = null, $attributes = [
                            'class' => 'pull-right',
                        ]) !!}
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection

@section('footer')
    {{--bootstrap select--}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

    {{--bootstrap filestyle--}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.1/bootstrap-filestyle.js"></script>

    {{--custom scripts--}}
    <script type="text/javascript">

        // Set file input style
        $(":file").filestyle({icon: false});
        // Accept only image formats
        $(":file").attr('accept', 'image/*');

        var inputfield = document.getElementById('name');
        inputfield.onkeyup = function () {
            document.getElementById('legend').innerHTML = "New Category: " + inputfield.value;
        };

        // Subtract btw from price
        $('#subtractBTW').click(function () {
            $priceInput = $('#price');
            $priceNew = $priceInput.val() * 0.79;
            $priceRounded = $priceNew.toFixed(3);
            $priceInput.val($priceRounded);
        });



        $(document).ready(function () {
            // Add new option "create new" to suggestion, distillery and tax
            $.each(['suggestions', 'distilleries', 'taxes', 'regions'], function (index, value) {
                $item = $('#' + value);

                // Add option groups to suggestions, distilleries and taxes lists + show respective modals
                if (value != 'regions') {
                    $item.wrapInner('<optgroup label="Existing" class="existing"></optgroup>');
                    $item.prepend('<optgroup label="Add New" class="new"><option value="0">Add new ' + value + '</option></optgroup>');

                    // React when "create new" is selected
                    $('select#' + value).change(function () {
                        console.log('input changed');
                        if ($(this).val() == 0) {
                            $options = {
                                'show': 'true',
                                'backdrop': 'true',
                                'keyboard': 'false'
                            };
                            $('#modal_' + value).modal($options);// Shows the correct modal
                            $('select#' + value).selectpicker('val', '-1');// reset the input in case no new item was added
                        }
                    });

                    // Add new item to drop down list
                    $('#modal_' + value + ' button.btn-primary').click(function () {
                        //Remove previous alert if present
                        $('#modal_' + value + ' .alert').remove();

                        //Set Validation rules
                        var a = 0; var b = 0;
                        a += ( jQuery.trim($('#' + value + '_name').val()).length > 0)? 1 : 0; b += 1;
                        a += ( jQuery.trim($('#' + value + '_description').val()).length > 0)? 1 : 0; b += 1;
                        if (value == 'distilleries') {
                            a += ( $('#regions').val() > 0 )? 1:0; b += 1;
                        }

                        // Check validation rules
                        if(a == b) {
                            console.log('success');
                            $name = $('#' + value + '_name').val();
                            $existingGroup = $('#' + value + ' .existing');
                            // Remove dummy content from list if present
                            $existingGroup.find('.dummy').remove();
                            $key = parseInt($existingGroup.children().last().attr('value')) + 1;
                            console.log($key);

                            // Add item to list
                            $existingGroup.append('<option value="' + $key + '" class="dummy">' + $name + '</option>');

                            // Update list
                            $('#' + value).selectpicker('refresh');

                            // Select new entry
                            $('#' + value).selectpicker('val', $key);
                            $('#' + value).val($key);

                            // Close modal
                            $('#modal_' + value).modal('hide');

                        } else {
                            console.log('fail');
                            $('#modal_' + value + ' fieldset').prepend('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> Looks like you didn&apos;t fill everything in.</div>')
                        }

                    });
                }

                $item.prepend('<option disabled selected value="-1">Please make a selection</option>');

                // Make suggestions and distilleries searchable
                $item.selectpicker({
                    liveSearch:true,
                    liveSearchNormalize:true,
                    liveSearchStyle:'contains',
                    width:'100%'
                });
            });
        });
    </script>
@endsection
