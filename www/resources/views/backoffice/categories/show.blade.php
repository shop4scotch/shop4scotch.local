@extends('layouts.app')

@section('header')

    {{-- Chartist --}}
    {{ Html::style('http://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css') }}

@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 style="display: inline-block;">{{ $category->name }}</h3>
            <div class="pull-right">
                <a href="{{ $category->id }}/edit" class="btn btn-link">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                {!! Form::open([
                    'method' => 'DELETE',
                    'route' => ['backoffice.categories.destroy', $category->id],
                    'style' => 'display:inline-block;'
                ]) !!}
                {{Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-link', 'style' => 'color:red; display:inline;'))}}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{ $category->name }}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{ $category->description }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <a href="{{ $category->id }}/edit" class="btn btn-link">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </a>
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['backoffice.categories.destroy', $category->id],
                'style' => 'display:inline-block;'
            ]) !!}
            {{Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-link', 'style' => 'color:red; display:inline;'))}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer')

@endsection
