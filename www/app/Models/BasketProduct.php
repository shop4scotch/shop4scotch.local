<?php

namespace App\Models;

use App\user;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BasketProduct extends Model
{
    // Relationships
    // =============

    protected $fillable = [
        'basket_id',
        'product_id',
        'amount'
    ];

    public function products() : BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }

}
