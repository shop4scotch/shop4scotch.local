<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace' => 'Api',
    'prefix' => 'api/v1',
], function () {
    $options = [
        'except' => [
            'create',
            'edit',
        ]
    ];

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');


    Route::resource('products', 'ProductsController', $options);
    Route::resource('categories', 'CategoriesController', $options);
    Route::resource('basket_products', 'BasketController', $options);
    Route::resource('product', 'ProductController', $options);



});

