<?php

namespace App\Http\Controllers\Api;


use App\Models\Basket;
use App\Models\BasketProduct;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;



class BasketController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BasketProduct::join('products', 'products.id', '=', 'basket_products.product_id')->get();
        //return Product::join('basket_product', 'products.id', '=', 'product_id')->where('basket_id', '=', 3)->get();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {

        $rules = [
            'basket_id' => 'required|integer',
            'product_id' => 'required|integer',
            'amount' => 'required|integer',
        ];

        $this->validate($request, $rules);
        $basket = new BasketProduct($request->only(['basket_id', 'product_id', 'amount']));

        if ($basket->save()) {


            return response()
                ->json($basket);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        return BasketProduct::join('products', 'products.id', '=', 'basket_products.product_id')->where('products.id', '=', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)

    {
        $product = BasketProduct::join('products', 'products.id', '=', 'basket_products.product_id')->where('products.id', '=', $id);

        if ($product) {
            if ($product->delete()) {
                return response()
                    ->json($product);

            }

            return response()
                ->json([
                    'error' => "Item `${id}` could not be deleted",
                ]);

        }

        return response()
            ->json([
                'error' => "Item `${id}` not found",
            ]);

    }
}
