<?php

namespace App\Http\Controllers\Api;

use App\Models\Basket;
use App\Models\Category;
use App\Models\Distillery;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;



class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('distillery', 'distillery.region', 'distillery.region.country', 'suggestion', 'tax')->get();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = Product::with('distillery', 'distillery.region', 'distillery.region.country', 'suggestion', 'tax', 'category')->find($id);

        return $product ?: response()
            ->json([
                'error' => "Product '$id' not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);

    }


    public function store(Request $request)
    {

        $rules = [
            'id' => 'required|integer',
        ];

        $this->validate($request, $rules);
        $basket = Basket::leftjoin('basket_products', 'basket_id', '=', 'baskets.id')->where('baskets.id', '=', 3)->get();

        $basketProduct = new $basket($request->only(['basket_id', 'product_id']));
        $product = Product::find($request->get('product')['id']);

        $basketProduct->product()->associate($product);


        if ($basketProduct->save()) {
            return response()
                ->json($basketProduct)
                ->setStatusCode(Response::HTTP_CREATED);
        }


        /*$product = Product::find($id);
         $basket = Basket::leftjoin('basket_product', 'basket_id', '=', 'baskets.id')->where('baskets.id', '=', 3)->get();

        $item = new $basket($request->only(['basket_id', 'product_id']));

        $item->$basket->associate($product);

        $item->save();
        */
        /*$rules = [
            'id' => 'required',

        ];

        $basket = Basket::leftjoin('basket_product', 'basket_id', '=', 'baskets.id')->where('baskets.id', '=', 3)->get();

        $this->validate($request, $rules);

        $item = new $basket($request->only(['basket_id', 'product_id']));

        $product = Product::find($request->get('id'));
        $item->category()->associate($product);


        if ($item->save()) {
            return response()
                ->json($item)
                ->setStatusCode(Response::HTTP_CREATED);
        }*/
    }
}
