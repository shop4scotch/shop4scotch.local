<?php

namespace App\Http\Controllers\Backoffice;

use App\Models\Basket;
use App\Models\Category;
use App\Models\Distillery;
use App\Models\Price;
use App\Models\Suggestion;
use App\Models\Tax;
use App\Models\Region;
use App\Models\Inventory;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.categories.index')->with('categories', Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        $category   = new Category();

        $data = [

            'category'    => $category
        ];
        return view('backoffice.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $rules = [

            'name'                     => 'required|unique:products|max:255',
            'description'              => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('backoffice/categories/create/')
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {
           
                        $category                     = new Category();
                        $category->name               = Input::get('name');
                        $category->description        = Input::get('description');
                        $category->save();
            
                    }
        

                // Show message
                Session::flash('message', 'Successfully created new Category: ' . Input::get('name') . ' !');

                // Redirect
                return redirect()->route('backoffice.categories.index'); // $ artisan route:list
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category      = Category::find($id);
        
       $inventoryData = Inventory::select([
            DB::raw('MONTH(created_at) AS date'),
            DB::raw('AVG(stock) AS stock')
        ])
            ->whereYear('created_at', '=', date('Y'))
            ->where('id', '=', $id)
            ->groupby('date')
            ->orderby('date', 'ASC')
            ->get();
        
        $inventory = array();
        for ($i = 0; $i < 12; ++$i) {
            if(!isset($inventoryData[$i])) {
                $inventoryData[$i] = null;
            }
            array_push($inventory, $inventoryData[$i]['stock'] );
        }


        $data = [
            'category'    => $category,
        ];

        return view('backoffice.categories.show', $data);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category    = Category::find($id);

        $data = [
            'category'    => $category,
        ];

        return view('backoffice.categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            // product validation rules
            'name'                     => 'required|unique:products|max:255',
            'description'              => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        // Process edited product
        if ($validator->fails()) {
            return Redirect::to('backoffice/categories/create/')
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {


            $category                     = Category::find($id);
            $category->name               = Input::get('name');
            $category->description        = Input::get('description');
            $category->save();
            

            // Show message
            Session::flash('message', 'Successfully edited category: ' . Input::get('name') . ' !');

            // Redirect
            return redirect()->route('backoffice.categories.index'); // $ artisan route:list
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return view('backoffice.categories.index')->with('categories', Category::all());
    }
}
