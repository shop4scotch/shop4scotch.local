<?php

use Illuminate\Database\Seeder;
use App\Models\Basket;
use App\Models\Product;
use Carbon\Carbon;

class BasketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Basket::class, 5)->create();
    }
}
