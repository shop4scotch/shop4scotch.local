Werkstuk NMDAD II 2MMPA-14:
===========================

Omschrijving
------------

Online webshop voor mobiel waar klanten whiskey kunnen kopen.
De app bevat ook een backoffice en API.


Documenten
----------

- [docs (folder)](docs)
    - [academische poster](docs/academische_poster.pdf)
    - [checklist](docs/checklist.md)
    - [presentatie](docs/presentatie.pdf)
    - [productiedossier](docs/productiedossier.pdf)
    - [timesheet Sam De Smedt](docs/timesheet_sam_de_smedt.xlsx)
    - [timesheet Stijn Meersschaert](docs/timesheet_stijn_meersschaert.xlsx)
- [www (folder)](www)


Wishlist met Features
---------------------

Dit zijn features die niet verplicht zijn, maar die we graag zouden implementeren indien alles vlot verloopt.

- samples gebaseerd op de aankoop
- recept of degustatie suggestie voor aankoop
- wereldkaart met producten
- blog over whiskey in het algemeen of bepaalde merken specifiek, recepten
- backoffice voor vertegenwoordigers
- verlanglijstje + account (+ delen op social media?)
- chat met support medewerkers
- ...


Auteurs
-------

| Sam De Smedt                                           | Stijn Meersschaert                                     |
|:-------------------------------------------------------|:-------------------------------------------------------|
| Github:    [samdesme](https://github.com/samdesme)     | Github:    [stijmeer](https://github.com/stijmeer)     |
| Bitbucket: [samdesme](https://bitbucket.org/samdesme/) | Bitbucket: [stijmeer](https://bitbucket.org/stijmeer/) |
| Email:     sam.desmedt@student.arteveldehs.be          | Email: stijn.meersschaert@student.arteveldehs.be |


Opleidingsinformatie
--------------------

| Informatie           |                                            |
|:---------------------|:-------------------------------------------|
| Opleidingsonderdeel  | New Media Design & Development II          |
| Academiejaar         | 2015-2016                                  |
| Opleiding            | Bachelor in de grafische en digitale media |
| Afstudeerrichting    | Multimediaproductie                        |
| Keuzeoptie           | proDEV                                     |
| Opleidingsinstelling | Arteveldehogeschool                        |


Benodigde gegevens
------------------

Er zijn momenteel geen bijzondere gegevens nodig om deze repo te bekijken.


Copyright & Licentie
--------------------

Copyright @ Sam De Smedt en Stijn Meersschaert